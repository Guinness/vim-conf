" .vimrc générique mis à votre disposition
" par les gentils membres actifs du Cr@ns
" Vous pouvez l'utiliser, le redistribuer, le modifier à votre convenance.
" Des questions, des suggestions : {nounou,ca}@lists.crans.org
" Licence : WTFPL

" Les sections commentées par "~" sont des features qui ne sont pas activées
" par défaut. Sentez-vous libre de les décommenter pour les utiliser.

"------------------------------------------------------------------------------
"
"
" +-----------+
" | Must-have |
" +-----------+

set nocompatible

set encoding=utf8

" +-----------+
" | Affichage |
" +-----------+

" Affiche la commande en train d'être tapée en bas à droite de l'écran
set showcmd

" Affiche le nom du fichier et le chemin dans le titre du terminal
set title

"~" " N'affiche pas sur plusieurs lignes les lignes qui sont plus grandes que
"     la fenêtre (comme sous nano)
"~" set nowrap

" ~~ Numérotation ~~
" Affiche 'ligne,n de caractère,colonne' en bas à droite du terminal
set ruler

" Affiche les numéros de lignes à gauche du terminal
set number

" Retire la marge à gauche sur les numéros de lignes
set numberwidth=2

" Utilise le presse papier système pour vim (utilise vim-gnome

set pastetoggle=<C-P>


" Souligne la ligne et la colonne du curseur
"
set cursorline
set cursorcolumn
" +-----------------+
" |  Backup et swp  |
" +-----------------+

set swapfile
set dir=~/tmp

" +-----------------+
" | Édition de code |
" +-----------------+

" ~~ Détection du fichier, auto indent ~~
" Autoindent c'est le mal, ça marche moins bien que indent

if has("autocmd")
    filetype plugin indent on
endif

" ~~ Coloration syntaxique ~~
" Active la coloration syntaxique
if has("syntax")
    syntax on
endif


" On change de colorscheme, liste sur https://code.google.com/p/vimcolorschemetest/

set t_Co=256

colorscheme 3dglasses_perso


" ~~ Indentation et tabulation ~~
" Conserve l'indentation de la ligne précédente lors d'un retour à la ligne
" Smart indent et preserveindent pour éviter de tout casser après
"
set preserveindent
set smartindent

"~" " Met les tabulations à 4 colonnes
set tabstop=4

"~" " Indente à 4 colonnes pour les opérations de réindentation
set shiftwidth=4

"~" " Remplace les (futures) tabulations par des espaces
set expandtab

"~" On veut des vrais tabulations dans les Makefile

autocmd Filetype make set noexpandtab tabstop=4 shiftwidth=2 nopi ci

" +---------------------------+
" | Recherche et substitution |
" +---------------------------+

" ~~ Recherche ~~
" Commence la recherche dès les premiers caractères tapés (comme sous less)
set incsearch

"~" Rend la recherche insensible à la casse, sauf si maj dans le motif

set ignorecase
set smartcase

"~" " Surligne les correspondances dans les recherches
"~" " (Exécuter nohl pour désactiver les hl (ils restent même une fois la recherche terminée))
set hlsearch

"~" Décolore les hl:
nmap <silent> <leader> /: nohlsearch<CR>

"~" Affiche une liste lors de la complétion de commandes/fichiers
set wildmode=longest,list:full


"+----------------------------+
"| Enregistrement et fermeture|
"+----------------------------+

" Enregistre automatiquement pour certaines commandes (style make)
set autowrite

" Met à jour automatiquement les fichiers modifiés hors de vim.
set autoread

" Plutôt que la commande fail à cause de changements non sauvegardés,
" Ouvre un dialogue qui demande si on veut sauvegarder avant
set confirm

" +--------+
" | Divers |
" +--------+

" Change la taille de l'historique des commandes (par défaut 20)
set history=500


"~" " Change le comportement de la TAB-complétion : on complète au plus long
"~" " au premier appui, on affiche la liste des possibilités au deuxième
"~" set wildmode=longest,list
" Format de la barre d'état (tronquée au début, fichier, flags,  :
set statusline=%<%f%m\ %r\ %h\ %w%=%l,%c\ %p%%


" +------------------+
" | Hacks et scripts |
" +------------------+

" ~~ Hack pour mettre en rouges les espaces indésirables en fin de ligne. ~~
" ~~ Ne gêne pas la vue en mode édition. ~~
" ~~ Adapté de http://vim.wikia.com/wiki/Highlight_unwanted_spaces ~~

 highlight EspaceFinLigne ctermbg=red guibg=red
 match EspaceFinLigne /\s\+$/
 autocmd BufWinEnter * match EspaceFinLigne /\s\+$/
 autocmd InsertEnter * match EspaceFinLigne /\s\+\%#\@<!$/
 autocmd InsertLeave * match EspaceFinLigne /\s\+$/
 autocmd BufWinLeave * call clearmatches()

" ~~  shebang automatique lors de l'ouverture nouveau
" ~~  d'un fichier *.py, *.sh (bash), modifier l'entête selon les besoins :
 autocmd BufNewFile *.sh,*.bash 0put =\"#!/bin/bash\<nl># -*- coding: UTF8 -*-\<nl>\<nl>\"|$
" autocmd BufNewFile *.py 0put=\"#!/usr/scripts/python.sh \"|1put=\"# -*- coding: UTF8 -*-\<nl>\<nl>\"|$

" ~~ Pour l'indentation python

autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd BufRead *.py set nocindent
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``

" ~~ Pour Vim en CoqIDE

let CoqIDE_coqtop = "/usr/bin/coqtop"
let g:CoqIDEDefaultKeyMap=1


" ~~ Coloration du fond après n colonnes ~~
" ~~ /!\ Seulement pour vim 7.3 et plus /!\ ~~
" ~~ Adapté de http://blog.hanschen.org ~~

highlight ColorColumn ctermbg=DarkGrey guibg=DarkGrey
if exists('+colorcolumn')
    execute "set colorcolumn=".join(range(81,335), ',')
endif


"~" Surligne les espaces insécables
au BufEnter * hi Nbsp ctermbg=233 guibg=black
au BufEnter * match Nbsp /\%uA0/
